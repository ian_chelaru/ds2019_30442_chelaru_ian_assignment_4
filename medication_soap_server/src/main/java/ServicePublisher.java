import services.ActivityServiceImpl;

import javax.xml.ws.Endpoint;

public class ServicePublisher
{
    public static void main(String args[])
    {
        Endpoint.publish("http://localhost:8081/activityservice",new ActivityServiceImpl());
    }
}
