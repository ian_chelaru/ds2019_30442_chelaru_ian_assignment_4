package dao.hibernate;

import dao.hibernate.util.HibernateUtil;
import entities.Activity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

public class HibernateActivityDao
{
    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public List<Activity> findAllByPatientId(int patientId)
    {
        Session currentSession = sessionFactory.openSession();
        Query query = currentSession.createQuery("from Activity as a where a.patientId = ?1");
        query.setParameter(1,patientId);
        List<Activity> prescriptionList = query.getResultList();
        currentSession.close();
        return prescriptionList;
    }


}
