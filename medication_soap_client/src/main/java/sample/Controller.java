package sample;

import javafx.fxml.FXML;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import jaxws.Activity;
import services.ActivityServiceClient;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

public class Controller
{
    private final static long HOUR = 3600000;

    private ActivityServiceClient activityServiceClient;

    private List<Activity> activityList;

    private List<Label> labels = new ArrayList<>();
    private List<RadioButton> radioButtons = new ArrayList<>();

    @FXML
    private BarChart<String,Number> activityChart;
    @FXML
    private CategoryAxis xAxis;
    @FXML
    private NumberAxis yAxis;
    @FXML
    private DatePicker datePicker;
    @FXML
    private VBox labelsBox;
    @FXML
    private VBox buttonsBox;
    @FXML
    private Button sendButton;
    @FXML
    private TextArea textArea;

    @FXML
    public void initialize() throws Exception
    {
        activityServiceClient = new ActivityServiceClient();
        activityList = activityServiceClient.getActivities();

        xAxis.setLabel("Activity");
        yAxis.setLabel("Minutes");

        displayAbnormalActivities();
    }

    private void displayAbnormalActivities()
    {
        List<Activity> activities = getAbnormalActivities();

        for (Activity activity : activities)
        {
            labels.add(crateLabel(activity));
            radioButtons.add(createRadioButton());
        }

        labelsBox.getChildren().addAll(labels);
        buttonsBox.getChildren().addAll(radioButtons);

        sendButton.setDisable(true);
    }

    private Label crateLabel(Activity activity)
    {
        return new Label(displayActivity(activity));
    }

    private String displayActivity(Activity activity)
    {
        String result = "";
        LocalDate date = convertDateToLocalDate(new Date(activity.getStart()));
        result += date + " ";
        result += activity.getName() + " ";
        long minutes = (activity.getEnd() - activity.getStart()) / 60000 + 1;
        result += "for " + minutes + " minutes";
        return result;
    }

    private RadioButton createRadioButton()
    {
        RadioButton radioButton = new RadioButton("Abnormal");
        radioButton.setOnAction(e -> {
            long countSelected = radioButtons.stream().filter(ToggleButton::isSelected).count();
            if (countSelected > 0)
            {
                sendButton.setDisable(false);
            }
            else
            {
                sendButton.setDisable(true);
            }
        });
        return radioButton;
    }

    @FXML
    private void sendRecommendation()
    {
        textArea.clear();
    }

    private List<Activity> getAbnormalActivities()
    {
        return activityList.stream().filter(a ->
        {
            long activityDuration = a.getEnd() - a.getStart();
            if ("Sleeping".equals(a.getName()) && (activityDuration > 12 * HOUR))
            {
                return true;
            }
            if ("Leaving".equals(a.getName()) && (activityDuration > 12 * HOUR))
            {
                return true;
            }
            return "Toileting".equals(a.getName()) && (activityDuration > HOUR);
        }).collect(Collectors.toList());
    }

    @FXML
    private void dateChanged()
    {
        Map<String,Long> activityMap = new HashMap<>();

        LocalDate selectedDate = datePicker.getValue();
        activityList.stream().filter(a ->
        {
            LocalDate activityDate = convertDateToLocalDate(new Date(a.getStart()));
            return activityDate.isEqual(selectedDate);
        }).forEach(a ->
        {
            String activityName = a.getName();
            if (activityMap.containsKey(activityName))
            {
                activityMap.put(activityName,activityMap.get(activityName) + a.getEnd() - a.getStart());
            }
            else
            {
                activityMap.put(activityName,a.getEnd() - a.getStart());
            }
        });
        updateChart(activityMap);
    }

    private void updateChart(Map<String,Long> activityMap)
    {
        activityChart.setVisible(false);
        activityChart.getData().clear();
        activityMap.forEach((key,value) ->
        {
            XYChart.Series<String,Number> series = new XYChart.Series<>();
            series.setName(key);
            series.getData().add(new XYChart.Data<>("",value / 60000 + 1));
            System.out.println(key + " " + (value / 60000 + 1));
            activityChart.getData().add(series);
        });
        activityChart.setVisible(true);
    }

    private LocalDate convertDateToLocalDate(Date date)
    {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }
}
